package com.asset.crops.services;

import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.models.User;

public class LoginService {

	// Check if a user having the entered credentials is available in the database
	public boolean isUserValid(User user) {

		if (DataBaseFunctions.userExists(user))

			return true;

		return false;

	}

	// Checks whether the currently logged in user is an admin or not
	// (The admin is the first registered user in the database ie. id=1)
	public boolean isAdmin(String username) {

		int id = DataBaseFunctions.isAdmin(username);
		if (id == 1)
			return true;

		return false;
	}
}
