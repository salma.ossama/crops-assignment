package com.asset.crops.services;


import java.util.ArrayList;

import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.main.Application;
import com.asset.crops.models.Crop;

public class CropService {

	// Returns all crops available in the database
	public ArrayList<String> viewAllCrops() {

		ArrayList<Crop> crops = DataBaseFunctions.selectCrops();
		ArrayList<String> cropsNames = new ArrayList<String>();

		for (Crop c : crops)
			cropsNames.add(c.getCropName());

		return cropsNames;
	}

	// inserts a specific crop into the database table crops
	public void addCrop(Crop c) {
		String insertQuery = "insert into " + Application.cropsDatabaseTable + " values('" + c.getCropName() + "','"
				+ c.getBotanicalName() + "'," + c.getCode() +",'"+Application.imagesPath+c.getImage()+ "');";
		DataBaseFunctions.insertCrop(insertQuery);
	}

	// Returns a specific crop identified by crop name
	public Crop viewCrop(String cropName) {

		String selectCrop = "Select * from " + Application.cropsDatabaseTable + " where crop_name='" + cropName + "';";
		Crop c = DataBaseFunctions.selectCrop(selectCrop);
		return c;
	}

	// Deletes a specififc crop identified by crop name
	public void deleteCrop(String cropName) {
		String deleteQuery = "delete from " + Application.cropsDatabaseTable + " where crop_name='" + cropName + "';";
		DataBaseFunctions.deleteCrop(deleteQuery);
	}

	// Updated a specific crop identidied by crop name
	public void updateCrop(String oldName, Crop c) {
		
		String updateQuery = "update " + Application.cropsDatabaseTable
				+ " set crop_name='" + c.getCropName()
				+ "',botanical_name='" + c.getBotanicalName() + "',code=" + c.getCode()+",image='"+Application.imagesPath+c.getImage() + "' where crop_name='" + oldName
				+ "';";
		
		DataBaseFunctions.updateCrop(updateQuery);
	}

}
