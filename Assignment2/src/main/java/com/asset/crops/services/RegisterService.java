package com.asset.crops.services;

import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.main.Application;
import com.asset.crops.models.User;

public class RegisterService {

	// Check if the username entered by the user doesn't exist in the database then
	// insert a new record in the database
	// with the corresponding name and password otherwise return false to indicate
	// that the username is already taken
	public boolean registerUser(User u) {

		if (!DataBaseFunctions.userNameExists(u.getName())) {
			StringBuilder insertQuery = new StringBuilder(
					"insert into " + Application.usersDatabaseTable + " values(DEFAULT,'");
			insertQuery.append(u.getName() + "','" + u.getPassword() + "');");
			DataBaseFunctions.insertUser(insertQuery.toString());
			return true;
		}

		return false;

	}

}
