package com.asset.crops.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.services.CropService;
import com.asset.crops.services.LoginService;

@WebServlet(urlPatterns = "/viewcrops.do")
public class ViewCropsServlet extends HttpServlet {

	private CropService service = new CropService();
	private LoginService loginService = new LoginService();

	// before loading viewCrops.jsp we check from the database if the recently
	// logged in user is an admin or not
	// and we set a boolean attribute to be checked in the viewCrops.jsp to decide
	// whether the add/update/delete crop
	// options should be visible to the user then we load the viewCrops.jsp page
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("crops", service.viewAllCrops());
		if (loginService.isAdmin((String) request.getSession().getAttribute("username")))
			request.getSession().setAttribute("isAdmin", true);
		else
			request.getSession().setAttribute("isAdmin", false);
		request.getRequestDispatcher("/WEB-INF/views/viewCrops.jsp").forward(request, response);
	}

	// if a user selects a crop from the dropdown list we save the chosen option in
	// the session and
	// redirect the user to a detailed page of the crop

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String cropName = request.getParameter("crop");
		request.getSession().setAttribute("selectedCrop", cropName);
		response.sendRedirect("/viewsinglecrop.do");

	}

}
