package com.asset.crops.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.models.User;
import com.asset.crops.services.LoginService;

@WebServlet(urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {

	private LoginService service = new LoginService();

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// Load login page when user chooses login option from home page
		request.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// Check if a user with the entered username and password exists in the database
		// create a session for the user
		// containing the username and password
		// otherwise set an error message that should be displayed in login.jsp
		User u = new User(request.getParameter("username"), request.getParameter("password"));
		if (service.isUserValid(u)) {
			request.getSession().setAttribute("username", u.getName());
			request.getSession().setAttribute("password", u.getPassword());
			response.sendRedirect("/viewcrops.do");
		} else {
			request.setAttribute("errorMessage", "Invalid credentials!");
			request.getRequestDispatcher("/WEB-INF/views/login.jsp").forward(request, response);

		}
	}

}