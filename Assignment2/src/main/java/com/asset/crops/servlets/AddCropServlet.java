package com.asset.crops.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.main.Application;
import com.asset.crops.models.Crop;
import com.asset.crops.services.CropService;

@WebServlet(urlPatterns = "/addcrop.do")
public class AddCropServlet extends HttpServlet {

	private CropService service = new CropService();

	// If the user (Admin) submits the add crop form in viewCrops.jsp
	// The new crop should be added to the database
	// then the updated list of crops should be displayed to the user
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Crop c = new Crop(request.getParameter("cropName"), request.getParameter("botanicalName"),
				Integer.parseInt(request.getParameter("code")),request.getParameter("image"));
		service.addCrop(c);
		request.setAttribute("crops", service.viewAllCrops());
		request.getRequestDispatcher("/WEB-INF/views/viewCrops.jsp").forward(request, response);
	}

}
