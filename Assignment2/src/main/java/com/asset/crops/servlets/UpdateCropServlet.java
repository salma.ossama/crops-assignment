package com.asset.crops.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.main.Application;
import com.asset.crops.models.Crop;
import com.asset.crops.services.CropService;

@WebServlet(urlPatterns = "/updatecrop.do")
public class UpdateCropServlet extends HttpServlet {

	CropService service = new CropService();
	
	
	public void init() throws ServletException {
		Application.logger = Logger.getLogger(UpdateCropServlet.class);

	}
	// When the user presses the update button in the viewSingleCrop page we
	// redirect him to updateCrop.jsp
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/views/updateCrop.jsp").forward(request, response);

	}

	// When the user submits the update form in updateCrop.jsp
	// we make sure he is providing data in all fields, if any of the fields
	// are empty we simply pass the old information of that field to the update
	// query of the database
	// then we redirect him to viewSingleCrop to view the updated information of the
	// crop
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String cropName = (String) request.getSession().getAttribute("selectedCrop");
		Crop oldCrop = service.viewCrop(cropName);
		String newCropName = request.getParameter("cropName");
		String newBotanicalName = request.getParameter("botanicalName");
		String image = request.getParameter("image");
		Application.logger.debug(image);
		int newCode;

		if (newCropName.equals(null) || newCropName.equals(""))
			newCropName = oldCrop.getCropName();
		if (newBotanicalName.equals(null) || newBotanicalName.equals(""))
			newBotanicalName = oldCrop.getBotanicalName();
		if (image.equals(null) || image.equals(""))
			image = oldCrop.getImage();
		if (request.getParameter("code").equals(null) || request.getParameter("code").equals(""))
			newCode = oldCrop.getCode();
		else
			newCode = Integer.parseInt(request.getParameter("code"));

		Crop c = new Crop(newCropName, newBotanicalName, newCode,image);
		request.getSession().setAttribute("selectedCrop", c.getCropName());

		service.updateCrop(cropName, c);
		response.sendRedirect("/viewsinglecrop.do");
	}

}
