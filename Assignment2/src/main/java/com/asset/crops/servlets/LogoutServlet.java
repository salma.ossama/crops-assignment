package com.asset.crops.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.main.Application;

@WebServlet(urlPatterns = "/logout.do")
public class LogoutServlet extends HttpServlet {

	// When user presses the logout button we invalidate the session and close the
	// database connection
	// and load the home page
	@Override
	public void init() throws ServletException {
		Application.logger = Logger.getLogger(LogoutServlet.class);

	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getSession().invalidate();
		try {
			DataBaseFunctions.connection.close();
			Application.logger.debug("Database connection closed!");
			DataBaseFunctions.connection=null;
		} catch (SQLException e) {
			Application.logger.debug("Couldn't close database connection!");
		}
		response.sendRedirect("/home.do");
	}

}
