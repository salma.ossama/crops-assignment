package com.asset.crops.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.models.User;
import com.asset.crops.services.RegisterService;

@WebServlet(urlPatterns = "/register.do")
public class RegisterServlet extends HttpServlet {

	private RegisterService service = new RegisterService();

	// Load register page when user presses the register button from the home page
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.getRequestDispatcher("/WEB-INF/views/register.jsp").forward(request, response);
	}

	// Check if the database contains a user with the provided credentials
	// display a username already exists error in register.jsp
	// otherwise redirect the user to the login page
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		User u = new User(request.getParameter("username"), request.getParameter("password"));
		boolean registered = service.registerUser(u);

		if (registered)
			response.sendRedirect("/login.do");
		else {
			request.setAttribute("registrationError", "Username already exists!");
			request.getRequestDispatcher("/WEB-INF/views/register.jsp").forward(request, response);
		}

	}

}
