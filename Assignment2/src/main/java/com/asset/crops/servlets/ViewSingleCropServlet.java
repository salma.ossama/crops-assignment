package com.asset.crops.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.models.Crop;
import com.asset.crops.services.CropService;

@WebServlet(urlPatterns = "/viewsinglecrop.do")
public class ViewSingleCropServlet extends HttpServlet {

	CropService service = new CropService();

	// When the user selects a specific crop from the dropdown list we fetch the
	// crop details from the database
	// and set attributes with these details to display them in the
	// viewSingleCrop.jsp
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cropName = (String) request.getSession().getAttribute("selectedCrop");
		Crop c = service.viewCrop(cropName);
		request.getSession().setAttribute("cropName", c.getCropName());
		request.getSession().setAttribute("botanicalName", c.getBotanicalName());
		request.getSession().setAttribute("code", c.getCode());
		request.getSession().setAttribute("image", c.getImage());
		request.getRequestDispatcher("/WEB-INF/views/viewSingleCrop.jsp").forward(request, response);
	}

	// When user (Admin) presses the delete button we delete from the database the
	// crop having
	// the name equal to the selected name from the dropdown list and redirect him
	// to the viewCrops page
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String cropName = (String) request.getSession().getAttribute("selectedCrop");
		service.deleteCrop(cropName);
		request.setAttribute("crops", service.viewAllCrops());
		request.getRequestDispatcher("/WEB-INF/views/viewCrops.jsp").forward(request, response);

	}

}
