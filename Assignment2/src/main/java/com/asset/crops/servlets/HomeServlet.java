package com.asset.crops.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.asset.crops.main.Application;

@WebServlet(urlPatterns = "/home.do")
public class HomeServlet extends HttpServlet {

	@Override
	public void init() throws ServletException {
		// Run application configurations defined in class App before handling any
		// requests
		Application app = new Application();
		app.config();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Start by loading the home page
		request.getRequestDispatcher("/WEB-INF/views/homePage.jsp").forward(request, response);

	}

}
