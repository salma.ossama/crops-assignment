package com.asset.crops.main;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.asset.crops.database.DataBaseFunctions;

public class Application {

	// Defining constants
	public static final String databaseURL = "jdbc:postgresql://localhost:5000/CropsDatabase";
	public static final String databaseUsername = "postgres";
	public static final String databasePassword = "Ss1234567";
	public static final String usersDatabaseTable = "users";
	public static final String cropsDatabaseTable = "crops";
	public static final String imagesPath = "images/";
	public static  Logger logger;
	
	
	public void config() {
		// Connection to database
		DataBaseFunctions db = new DataBaseFunctions();
		db.connectToDatabase();
		
		//Configure log4J
		String log4Jpath = System.getProperty("user.dir")+"/log4j.properties";
		PropertyConfigurator.configure(log4Jpath);
	}

}
