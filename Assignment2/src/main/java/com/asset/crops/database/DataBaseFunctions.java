package com.asset.crops.database;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.asset.crops.main.Application;
import com.asset.crops.models.Crop;
import com.asset.crops.models.User;

public class DataBaseFunctions {

	public static Connection connection;
	
	public DataBaseFunctions() {
		Application.logger = Logger.getLogger(DataBaseFunctions.class);
	}

	// Establish database connection
	public void connectToDatabase() {

		try {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(Application.databaseURL, Application.databaseUsername,
					Application.databasePassword);
			if (connection != null)
				Application.logger.debug("Connected to the database successfully!");

		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");
		}

	}

	// newly registered user should be inserted in the users table
	public static void insertUser(String insertQueries) {
		Statement s = null;
		try {
			s = connection.createStatement();
			s.executeUpdate(insertQueries);
		} catch (NullPointerException e) {
			Application.logger.debug("Connection to database failed!");
		}

		catch (Exception e) {
			Application.logger.debug("Username already exists");

		}
		;

	}

	// when a user registers, we check if the username provided already exists in
	// the database
	public static boolean userNameExists(String username) {
		String selectQuery = "Select * from " + Application.usersDatabaseTable + " where username='" + username + "';";
		Statement stmt;
		ResultSet rs = null;
		int size = 0;
		try {
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(selectQuery);

			if (rs != null) {
				rs.last();
				size = rs.getRow();
			}

		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");

		}

		if (size != 0)
			return true;
		return false;
	}

	// when the user logs in, we check if the credentials provided are valid
	public static boolean userExists(User u) {
		String selectQuery = "Select * from " + Application.usersDatabaseTable + " where username='" + u.getName()
				+ "' AND password='" + u.getPassword() + "';";
		Statement stmt;
		ResultSet rs = null;
		int size = 0;
		try {
			stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
			rs = stmt.executeQuery(selectQuery);

			if (rs != null) {
				rs.last(); // moves cursor to the last row
				size = rs.getRow(); // get row id
			}

		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");
		}

		if (size != 0)
			return true;
		return false;

	}

	// selecting all crops to be displayed in the dropdown ls=ist
	public static ArrayList<Crop> selectCrops() {
		Statement s;
		ArrayList<Crop> crops = new ArrayList<Crop>();
		try {
			s = connection.createStatement();

			ResultSet res = s.executeQuery("SELECT  * FROM " + Application.cropsDatabaseTable);
			while (res.next()) {
				Crop c = new Crop(res.getString("crop_name"), res.getString("botanical_name"),
						Integer.parseInt(res.getString("code")),res.getString("image"));
				crops.add(c);

			}
		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");
		}

		return crops;

	}

	// selecting a specific crop to be displayed in the viewSingleCrop view
	public static Crop selectCrop(String selectQuery) {
		Statement s;
		Crop c = null;
		try {
			s = connection.createStatement();

			ResultSet res = s.executeQuery(selectQuery);
			res.next();
			c = new Crop(res.getString("crop_name"), res.getString("botanical_name"),
					Integer.parseInt(res.getString("code")),res.getString("image"));

		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");
			
		}

		return c;

	}

	// when admin adds a new crop to the dropdown list we insert it in the crops
	// table
	public static void insertCrop(String insertQueries) {
		Statement s = null;
		try {
			s = connection.createStatement();
			s.executeUpdate(insertQueries);
		} catch (NullPointerException e) {
			System.out.println("Connection to database failed!");
		}

		catch (Exception e) {
			Application.logger.debug("Crop already exists");

		}
		;

	}

	// Admin can delete a crop from the database
	public static void deleteCrop(String deleteQuery) {
		Statement s = null;
		try {
			s = connection.createStatement();
			s.executeUpdate(deleteQuery);
		} catch (NullPointerException e) {
			Application.logger.debug("Connection to database failed!");
		}

		catch (Exception e) {
			Application.logger.debug("Couldn't delete your crop");

		}
		;
	}

	// Admin can update crop in the database
	public static void updateCrop(String updateQuery) {
		Statement s = null;
		

		
		try {
			s = connection.createStatement();				
			s.executeUpdate(updateQuery);
		} catch (NullPointerException e) {
			Application.logger.debug("Connection to database failed!");
		}
	

		catch (Exception e) {
			Application.logger.debug("Couldn't update your crop");

		}
		;
	}

	// Before giving the user the option to update,add or delete crop
	// we should check whether this user is an admin or not
	// admin is the first one registered on the system with id= 1

	public static int isAdmin(String username) {
		Statement s;
		int id = 0;
		try {
			s = connection.createStatement();

			ResultSet res = s.executeQuery(
					"select * from " + Application.usersDatabaseTable + " where username='" + username + "';");
			res.next();
			id = Integer.parseInt(res.getString("id"));

		} catch (Exception e) {
			Application.logger.debug("Connection to database failed!");
		}
		return id;

	}

}
