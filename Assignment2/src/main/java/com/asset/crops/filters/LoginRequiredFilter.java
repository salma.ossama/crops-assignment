package com.asset.crops.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.asset.crops.database.DataBaseFunctions;
import com.asset.crops.main.Application;

@WebFilter(urlPatterns = "*.do")
public class LoginRequiredFilter implements Filter {

	Application app = new Application();

	@Override
	public void destroy() {

	}

	@Override
	// intercept all requests with url patterns *.do
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) servletRequest;
		HttpServletResponse response = (HttpServletResponse) servletResponse;
		String requestURI = request.getRequestURI();

		// The below check is for auto-logout when the session expires
		// if the session hasn't expired yet
		// or the user requests getting to the homepage, the login page or the register
		// page
		// then he should be redirected to the requested page successfully
		// otherwise, if the session is expired the user should be redirected to the
		// login page
		if (requestURI.equals("/") || requestURI.contains("/home") || requestURI.contains("login")
				|| requestURI.contains("register") || request.getSession().getAttribute("username") != null) {

			// If the user logs out and tries to login again a new database connection is
			// created
			if (DataBaseFunctions.connection == null)
				app.config();

			chain.doFilter(servletRequest, servletResponse);
		} else {
			request.setAttribute("errorMessage", "");
			response.sendRedirect("/login.do");
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {

	}

}
