package com.asset.crops.models;

public class Crop {

	private String cropName;
	private String botanicalName;
	private int code;
	private String image;

	public Crop(String cropName, String botanicalName, int code,String image) {

		this.cropName = cropName;
		this.botanicalName = botanicalName;
		this.code = code;
		this.image = image;

	}

	public String getCropName() {
		return cropName;
	}

	public void setCropName(String cropName) {
		this.cropName = cropName;
	}

	public String getBotanicalName() {
		return botanicalName;
	}

	public void setBotanicalName(String botanicalName) {
		this.botanicalName = botanicalName;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

}
