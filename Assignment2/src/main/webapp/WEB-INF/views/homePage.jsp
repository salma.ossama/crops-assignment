<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style><%@include file="/WEB-INF/CSS/headerStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/homeStyle.css"%></style>
<title>Namaa|Welcome</title>


</head>

<header>
	<div class="wrapper">
		<div class="logo">
			<img src="resources/logo.png" alt=""><br> <a>Namaa</a>
		</div>

		<ul class="nav-area">
		
			<li><a onclick="showInfo()" id="aboutUs" style="display: block;">About us</a></li>
			<li><a onclick="showForm()" id="contactUs" style="display: block;">Contact us</a></li>

		</ul>

	</div>




 
<div id="welcome" class="welcome-text"  style="display: block;">
<h1>WE CELEBRATE THE JOY OF AGRICULTURE</h1> 
<a href="/register.do">Register</a>
<a href="/login.do">Login</a></div>

</header>


<body>

<div id="info" class="container" style="display: none;">

<div class="img" >
<center><h1>Namaa</h1><center>
</div>

<p> Namaa is already the world’s largest farmer-to-farmer digital network, with more than 
1 million farmers using our ecosystem in Kenya and Uganda alone. Our farmers share more than 40,000 
questions and answers every day.

Farmers connect with one another to solve problems, share ideas, and spread innovation, for free, and 
withUtilising the latest machine learning technology, Namaa’s service gets bespoke, crowd- sourced information to help farmers increase yields, 
gain insight into pricing, tackle the effects of climate change, 
source the best quality seeds, fertiliser, and loans and diversify their agricultural interests.</p>

</div>

<div id="form" class="container" style="display: none;">

<div class="img" >
<center><h1>Namaa</h1><center>
</div>

<div class="contact-section">
 <form class="contact-form" action="index.html" method="post">
    <input type="text" class="contact-form-text" placeholder="Name">
    <input type="email" class="contact-form-text" placeholder="E-mail">
    <input type="text" class="contact-form-text" placeholder="Phone Number">
    <textarea class="contact-form-text" placeholder="Message"></textarea>
    <input type="submit" class="contact-form-btn" value="Send">
  </form>
 
</div>

</div>

<script src="/JS/homePage.js" type="text/javascript"></script>
</body>
</html>