<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style><%@include file="/WEB-INF/CSS/formStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/headerStyle.css"%></style>
<title>Namaa|Register</title>
</head>

<header>
<div class="wrapper">
<div class="logo">
<img src="resources/logo.png" alt=""><br>
<a>Namaa</a>
</div>
</div>

</header>

<body>

<form action = "/register.do" method="post">
<div class="form-box">
<h1>Register</h1>


<div class="textbox">
    <i class="fas fa-user"></i>
    <input type="text" placeholder="Username" name="username" required>
</div>

 <div class="textbox">
    <i class="fas fa-lock"></i>
    <input type="password" placeholder="Password" name="password" required>
  </div>
  
    <input type="submit" class="btn" value="Register">
  

<p><font color="red">${registrationError}</font></p>
</div>
</form>
</body>
</html>