<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style><%@include file="/WEB-INF/CSS/headerStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/formStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/viewCropsStyle.css"%></style>

<title>Namaa|Crops</title>
</head>

<header>
	<div class="wrapper">
		<div class="logo">
			<img src="resources/logo.png" alt=""><br> <a>Namaa</a>
		</div>

		<ul class="nav-area">
			<c:if test="${isAdmin}">
			<li><a onclick="showForm()" id="b1" style="display: block;">Add Crop</a></li></c:if>
			<li><a onclick="showForm()" id="b2" style="display: none;">View Crops</a></li>
			<li><a href="/logout.do">Logout</a></li>

		</ul>

	</div>



</header>
<body>





	<div class="box" id="select" style="display: block;">
		<form action="/viewcrops.do" method="post">
		<select name="crop" onchange="this.form.submit()">
			<option style="display: none" selected="selected" value="">
				View all crops</option>
			<c:forEach items="${crops}" var="crop">
				<option>${crop}</option>
			</c:forEach>
		</select>
		</form>
	</div>

	<div style="display: none;" id="cropForm">
		<form action="/addcrop.do" method="post">
			<div class="form-box">
				<h1>Add New Crop</h1>
				<div class="textbox">
					<input type="text" placeholder="Crop Name" name="cropName" required/>
				</div>
				<div class="textbox">
					<input type="text" placeholder="Botanical Name"
						name="botanicalName" required/>
				</div>
				<div class="textbox">
					<input type="text" placeholder="Code" name="code" required/>
				</div>
				<div class="textbox">
				<input type="file"  name="image">
			</div>
				
				<input type="submit" class="btn" value="Add Crop" />
			</div>
		</form>

	</div>
<script src="/JS/viewCrops.js" type="text/javascript"></script>
</body>
</html>