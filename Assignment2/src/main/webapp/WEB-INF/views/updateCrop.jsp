<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style><%@include file="/WEB-INF/CSS/headerStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/formStyle.css"%></style>
<title>Namaa|Update</title>
</head>

<header>
	<div class="wrapper">
		<div class="logo">
			<img src="resources/logo.png" alt=""><br> <a>Namaa</a>
		</div>

		<ul class="nav-area">
			<li><a href="/viewcrops.do">View Crops</a></li>
			<li><a href="/logout.do">Logout</a></li>

		</ul>

	</div>


</header>

<body>

	<form action="/updatecrop.do" method="post">
		<div class="form-box">
			<h1>Update</h1>

			<div class="textbox">

				<input type="text" placeholder="Crop Name" name="cropName">
			</div>

			<div class="textbox">

				<input type="text" placeholder="Botanical Name" name="botanicalName">
			</div>

			<div class="textbox">

				<input type="text" placeholder="Code" name="code">

			</div>
			
				<div class="textbox">

				<input type="file"  name="image">
			

			</div>


			<input type="submit" class="btn" value="Update">





		</div>

	</form>
</body>
</html>