<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style><%@include file="/WEB-INF/CSS/headerStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/formStyle.css"%></style>
<style><%@include file="/WEB-INF/CSS/viewSingleCropStyle.css"%></style>
<title>Namaa|${cropName}</title>
</head>

<header>
	<div class="wrapper">
		<div class="logo">
			<img src="resources/logo.png" alt=""><br> <a>Namaa</a>
		</div>

		<ul class="nav-area">
			<li><a href="/viewcrops.do">View Crops</a></li>
			<li><a href="/logout.do">Logout</a></li>

		</ul>

	</div>


</header>


<body>



	<div class="form-box">
		<h1>${cropName} <img id="fruit" src="${image}" alt=""> </h1>


		
		<div class="textbox">
	 	<p>Crop Name</p>
		${cropName}
		</div>



		
		<div class="textbox">
		<p>Botanical Name</p>
		${botanicalName}
		</div>

		
		
		<div class="textbox">
		<p >Code </p>	
		${code}
		</div>

		<c:if test="${isAdmin}">
		<form action="/viewsinglecrop.do" method="post">


			<input type="submit" class="btn" value="Delete">
		</form>


		<form action="/updatecrop.do">
			<input type="submit" class="btn" value="Update">
		</form>
		</c:if>







	</div>


</body>
</html>